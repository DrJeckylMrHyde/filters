package pl.javastart.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "loginServlet", value = "/login")
public class LoginServlet extends HttpServlet {

    private static final String USERNAME = "admin";
    private static final String PASS = "admin";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("Login Servlet doPost()");

        String userParam = request.getParameter("username");
        String passParam = request.getParameter("password");

        if(validate(userParam,passParam)){
            request.getSession(true).setAttribute("username",userParam);
        }
        response.sendRedirect("admin.jsp");
    }

    private boolean validate(String username, String password) {
        return USERNAME.equals(username) && PASS.equals(password);
    }
}
