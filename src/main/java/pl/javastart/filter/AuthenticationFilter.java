package pl.javastart.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        System.out.println("Authentication filter");
        HttpServletRequest httpServletRequest
                = (HttpServletRequest) servletRequest;
        HttpSession session = httpServletRequest.getSession(false);

        if(session != null
                && session.getAttribute("username") != null){
            System.out.println("Session + user valid");
            filterChain.doFilter(httpServletRequest,servletResponse);
        } else {
            System.out.println("Session or user invalid");
            HttpServletResponse httpServletResponse
                    = (HttpServletResponse) servletResponse;
            httpServletResponse.sendRedirect("login.jsp");
        }
    }

    @Override
    public void destroy() {

    }
}
