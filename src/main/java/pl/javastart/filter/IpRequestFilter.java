package pl.javastart.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class IpRequestFilter implements Filter {

    private String ipPattern;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
//        wzor adresu ip skonfigurowany jest w web.xml
        ipPattern = filterConfig.getInitParameter("ipPattern");

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        String requestIp = request.getRemoteAddr();
        System.out.println("Ip filter " + requestIp);

        if(requestIp.matches(ipPattern)){
            System.out.println("IP ok");
            filterChain.doFilter(request,response);
        } else {
            System.out.println("IP not ok");
            HttpServletResponse httpServletResponse
                    = (HttpServletResponse) response;
            httpServletResponse.sendError(403);
        }
    }

    @Override
    public void destroy() {

    }
}
